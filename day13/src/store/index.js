import Vue from 'vue';
import Vuex from 'vuex';
import mutations from "./mutations"
import storage from '../utils/storage'; 
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    userInfo : storage.getItem("userInfo") || null,
    menuList : storage.getItem("menuList") || null,
    actionList : storage.getItem("actionList") || null,
  },
  mutations,
  actions: {
  },
  modules: {
  },
});
