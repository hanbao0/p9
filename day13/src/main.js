import Vue from 'vue';
import ElementUI from 'element-ui';
import App from './App.vue';
import router from './router';
import store from './store';

// 引入element-ui相关
import 'element-ui/lib/theme-chalk/index.css';

// 注册element-ui的
Vue.use(ElementUI);

Vue.config.productionTip = false;

//引入storage
import storage from "./utils/storage"

//将storage进行全局注册
Vue.prototype.$storage = storage

//引入userApi
import userApi from "./api/user"
//引入menuApi
import menuApi from "./api/menu"

//将userApi挂载到全局
Vue.prototype.userApi = userApi;
//将menuApi挂载到全局
Vue.prototype.menuApi = menuApi;

//检测当前的环境
console.log(process.env)

//日期格式化时间
import moment from "moment"
//将日期格式化时间挂载到全局
Vue.prototype.moment = moment;


new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
