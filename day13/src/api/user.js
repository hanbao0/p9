//引入axios请求文件
import request from "../utils/request"

//导出封装的api接口
export default {
    //登录接口
    login(data){
        return request({
            url : "/users/login",
            method : "POST",
            data
        })
    },
    //获取菜单权限接口
    getPermissionList(){
        return request({
            url : "/users/getPermissionList",
            method : "GET"
        })
    },
    //获取用户管理列表接口
    getUserList(data){
        return request({
            url : "/users/list",
            method : "GET",
            data
        })
    },
    //单个用户删除以及批量删除用户接口
    removeUser(userIds){
        return request({
            url : "/users/delete",
            method : "POST",
            data : {userIds}
        })
    },
    //获取角色列表接口
    getRoleList(){
        return request({
            url : "/roles/allList",
            method : "GET"
        })
    },
    //获取部门列表接口
    getDeptList(){
        return request({
            url : "/dept/list",
            method : "GET"
        })
    },
    //新增用户接口
    addUser(data){
        return request({
            url : "/users/operate",
            method : "POST",
            data 
        })
    }

}