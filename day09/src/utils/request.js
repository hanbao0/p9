// axios二次封装


// 1.引入axios
import axios from "axios"

// 2.引入环境变量文件
import config from "../config/index"

// 3.创建axios实例对象，并添加公共请求配置
const service = axios.create({
    // 请求的公共地址
    baseURL: config.baseApi,
    // 请求超时时间
    timeout: 8000
})

//4. 创建请求拦截器
service.interceptors.request.use((req) => {
    // 通用业务token发送 loading加载
    let headers = req.headers;
    if (!headers.Authorization) headers.Authorization = "要发送的token"
    return req
}, (err) => {
    return Promise.reject(err)
})

//5.创建请求拦截器
//5. 创建响应拦截器
service.interceptors.response.use((res) => {
        const {
            code,
            data,
            msg
        } = res.data;
        if (code === 200) {
            return data;
        } else if (code === 40001) {
            Message.error(TOKEN_INVALID);
            setTimeout(() => {
                router.push("/login");
            }, 1500)
            return Promise.reject(TOKEN_INVALID);
        } else {
            Message.error(msg || NETWORK_ERROR)
            return Promise.reject(msg || NETWORK_ERROR)
        }
    })
    //10. 定义请求核⼼函数
function request(options) {
    options.method = options.method || "get";

    if (options.method.toLowerCase() == "get") {
        options.params = options.data;
    }

    if (config.env === "prod") {
        service.defaults.baseURL = config.baseApi
    } else {
        service.defaults.baseURL = config.mock ? config.mockApi : config.baseApi
    }

    return service(options)
}
//12. 处理使⽤this.request.get/post/put/delete/patch这⼏种请求⽅法
/*
 this.$request.get("/login",{name : "jack"},{mock:true, loading :
true}).then(res=>{
 console.log(res)
 })
*/
["get", "post", "put", "delete", "patch"].forEach((item) => {
    request[item] = (url, data, options) => {
        return request({
            url,
            data,
            method: item,
            ...options
        })
    }

})

export default request