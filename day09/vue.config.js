module.exports = {
    //配置vue项目打包白屏问题
    publicPath: "./",
    // 配置服务器
    devServer: {
        // 设置端口号为8888
        port: 8888,
        // 设置主机名
        host: "localhost",
        // 设置启动项目时自动打开浏览器
        open: true,
        // 关闭https
        https: false
    },
    // 关闭eslint
    lintOnSave: false,
}