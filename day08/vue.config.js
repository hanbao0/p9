module.exports = {
    //解决vue项目打包白屏问题
    publicPath: "./",
    //对服务器进行配置
    devServer : {
        //配置的端口号
        port : 8888,
        //配置启动项目时自动打开浏览器运行项目
        open : true,
        //配置主机名
        host : "localhost",
        //是否开启https
        https : false,
        //配置跨域
        proxy : {
            //代理名称
            [process.env.VUE_APP_BASE_API]:{
                //跨域的地址
                target : process.env.VUE_APP_SERVICE_URL,
                //开启跨域
                changeOrigin : true,
                //路径重写
                pathRewrite : {
                    ["^" + process.env.VUE_APP_BASE_API] : ""
                }
            }
        }
    },
    //关闭eslint
    lintOnSave : false,
    //配置打包时不生成.map后缀名的文件
    productionSourceMap : false
}