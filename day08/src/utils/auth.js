//常量
const TOKEN_KEY = "token";
const USER_KEY = "user"

//存储token
function setToken(token){
    sessionStorage.setItem(TOKEN_KEY,token)
}
//获取token
function getToken(){
    return sessionStorage.getItem(TOKEN_KEY)
}
//存储用户信息
function setUserInfo(userInfo){
    sessionStorage.setItem(USER_KEY,JSON.stringify(userInfo))
}
//获取用户信息
function getUserInfo(){
    return JSON.parse(sessionStorage.getItem(USER_KEY))
}
//删除token和用户信息
function removeTokenAndUserInfo(){
    sessionStorage.removeItem(TOKEN_KEY)
    sessionStorage.removeItem(USER_KEY)
}
export default {
    setToken,
    getToken,
    setUserInfo,
    getUserInfo,
    removeTokenAndUserInfo
}




