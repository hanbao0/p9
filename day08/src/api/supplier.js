/**
 * 
 * 供应商相关的api接口: 供应商列表 、 新增供应商 、 删除供应商、 修改供应商
 * 
 */

import request from "../utils/request"

// 供应商列表

let getSupplierList = (page = 1, size = 10, data) => {
    return request({
        url: `/supplier/list/${page}/${size}`,
        method: 'GET',
        data
    })
}

// 删除供应商

let deleteSupplier = (id) => {
        return request({
            url: `/supplier/${id}`,
            method: 'DELETE'
        })
    }
    //新增供应商接口
let addSupplier = (data) => {
        return request({
            url: "/supplier",
            method: "POST",
            data
        })
    }
    //查询单个供应商的数据
let findSupplier = (id) => {
    return request({
        url: `/supplier/${id}`,
        method: "GET",
    })
}

//供应商编辑方法
let editSupplier = (id, data) => {
    return request({
        url: `/supplier/${id}`,
        method: "PUT",
        data
    })
}
export default {
    getSupplierList,
    deleteSupplier,
    addSupplier,
    editSupplier,
    findSupplier,
}