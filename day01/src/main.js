import Vue from 'vue';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import App from './App.vue';
import router from './router';
import store from './store';

Vue.config.productionTip = false;

import axios from "./utils/request"
Vue.prototype.$axios = axios;
Vue.use(ElementUI);
new Vue({
    router,
    store,
    ElementUI,
    render: (h) => h(App),
}).$mount('#app');