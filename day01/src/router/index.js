import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';

Vue.use(VueRouter);

const routes = [{
        path: '/',
        name: 'Home',
        component: () =>
            import ('../views/Home.vue'),
        children: [{
                path: '/Member',
                name: 'Member',
                component: () =>
                    import ('../views/Member.vue'),
            },
            {
                path: '/shouhome',
                name: 'shouhome',
                component: () =>
                    import ('../views/shouhome.vue'),
            },
            {
                path: '/supplier',
                name: 'supplier',
                component: () =>
                    import ('../views/supplier.vue'),
            },
            {
                path: '/commodity',
                name: 'commodity',
                component: () =>
                    import ('../views/commodity.vue'),
            },
            {
                path: '/staff',
                name: 'staff',
                component: () =>
                    import ('../views/staff.vue'),
            },
        ]
    },
    {
        path: '/about',
        name: 'About',
        component: () =>
            import ('../views/About.vue'),
    },
    {
        path: '/login',
        name: 'login',
        component: () =>
            import ('../views/login.vue'),
    },
];

const router = new VueRouter({
    routes,
});

export default router;


router.beforeEach((to, from, next) => {
    console.log(to.path);

    // //获取本地存储中的token值
    let token = localStorage.getItem("token");

    //判断当前页面如果是登陆页面的话，判断token是否存在，
    if (to.path == "/login") {
        //如果存在，跳转到后台管理页面
        if (token != null) {
            next("/");
        }
    } else { //非登录页面，如果不是登陆页面，判断token是否存在，
        //如果不存在，跳转到登陆页面
        if (token == null) {
            next("/login")
        }
    }

    //非登录页面，如果不是登陆页面，判断token是否存在，
    if (to.path != "/login") {
        //如果不存在，跳转到登陆页面
        if (token == null) {
            next("/login")
        }
    }

    next(); //是否跳转的页面
})