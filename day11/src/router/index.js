import Vue from 'vue';
import VueRouter from 'vue-router';
import login from "../views/login.vue"


Vue.use(VueRouter);

const routes = [{
        path: '/login',
        name: 'login',
        component: login,
    },
    {
        path: '/',
        name: '',
        meta: {
            title: "首页"
        },
        component: () =>
            import ('../components/Home.vue'),
        redirect: "/welcome",
        children: [{
                path: "/welcome",
                name: "welcome",
                meta: { title: "欢迎体验vue3全栈课程" },
                component: () =>
                    import ('../views/welcome.vue'),
            },
            {
                path: "/system/user",
                name: "user",
                meta: { title: "用户管理" },
                component: () =>
                    import ('../views/user.vue'),
            },
            {
                path: "/system/menu",
                name: "menu",
                meta: { title: "菜单管理" },
                component: () =>
                    import ('../views/menu.vue'),
            },
            {
                path: "/system/role",
                name: "role",
                meta: { title: "角色管理" },
                component: () =>
                    import ('../views/role.vue'),
            },
            {
                path: "/system/dept",
                name: "dept",
                meta: { title: "部门管理" },
                component: () =>
                    import ('../views/dept.vue'),
            },
            {
                path: "/audit/leave",
                name: "leave",
                meta: { title: "休假申请" },
                component: () =>
                    import ('../views/leave.vue'),
            },
            {
                path: "/audit/approve",
                name: "approve",
                meta: { title: "待我审批" },
                component: () =>
                    import ('../views/approve.vue'),
            },
        ]
    },
];

const router = new VueRouter({
    routes,
});

export default router;