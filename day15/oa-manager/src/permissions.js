import router from "./router"

router.beforeEach((to,from,next)=>{
    if(checkPermission(to.path)){
        next()
    }else{
        next({path : "/404"})
    }
})


//判断要进入的路由是否在所有的路由里面存在,如果存在,则给函数返回为true,否在返回为false
function checkPermission(path){
   
    //1. 要进入的路由已经获取到了 , path

    //2. 获取到所有的路由
    let allRoutes = router.getRoutes()

    //3. 过滤进入路由是否在所有路由中存在
    let hasPermission = allRoutes.filter((item,index)=> {
        return item.path == path
    })


    if(hasPermission.length > 0){
        return true
    }else{
        return false;
    }
}