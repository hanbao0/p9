import Vue from 'vue';
import VueRouter from 'vue-router';
import Login from "../views/Login.vue"
Vue.use(VueRouter);

const routes = [
  //登录路由
  {
    path : "/login",
    name : "login",
    component : Login
  },
  {
    path : "/test",
    name : "test",
    component : () => import("../views/Test.vue")
  },
  {
    path : "/404",
    name : "404",
    component :  () => import("../views/404.vue")
  },
  {
    path : "*",
    redirect : "/404"
  },
  //主页路由
  {
    path : "/",
    name : "home",
    meta : {title : "首页"},
    component : ()=> import("../components/Home.vue"),
    redirect : "/welcome",
    children : [
      {
        path : "/welcome",
        name : "welcome",
        meta : {title : "欢迎体验Vue3全栈课程"},
        component : ()=> import("../views/Welcome.vue")
      },
      {
        path : "/system/user",
        name : "user",
        meta : {title : "用户管理"},
        component : ()=> import("../views/User.vue")
      },
      {
        path : "/system/menu",
        meta : {title : "菜单管理"},
        name : "menu",
        component : ()=> import("../views/Menu.vue")
      },
      {
        path : "/system/role",
        name : "role",
        meta : {title : "角色管理"},
        component : ()=> import("../views/Role.vue")
      },
      {
        path : "/system/dept",
        name : "dept",
        meta : {title : "部门管理"},
        component : ()=> import("../views/Dept.vue")
      },
      {
        path : "/audit/leave",
        name : "leave",
        meta : {title : "休假申请"},
        component : ()=> import("../views/Leave.vue")
      },
      {
        path : "/audit/approve",
        name : "approve",
        meta : {title : "待我审批"},
        component : ()=> import("../views/Approve.vue")
      }
    ]
  }
];

const router = new VueRouter({
  routes,
});

console.log(router.getRoutes())

export default router;
