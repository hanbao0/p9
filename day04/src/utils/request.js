// 引入axios
import axios from 'axios'

import srore from '../store'
//创建axios实例
const request =axios.create({
    baseURL:process.env.VUE_APP_BASE_API,
    timeout:5000
})
// 创建请求拦截器
request.interceptors.request.use(function (config) {
  // 获取vuex里面的token
  let token = srore.state.token
  // 将获取过来的token传输到后台
  config.headers.token=token
  
    return config;
  }, function (error) {
    return Promise.reject(error);
  });

// 创建响应拦截器
request.interceptors.response.use(function (response) {
    return response;
  }, function (error) {
    return Promise.reject(error);
  });

//   导出 实例
  export default request