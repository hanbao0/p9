// 定义常量
const TOKEN_KEY ="token"
const USER_KEY="user"

// 设置存储token
function setToken(token){
sessionStorage.setItem(TOKEN_KEY,token)
}

// 获取token
function getToken(){
    sessionStorage.getItem(TOKEN_KEY)
}

// 存储用户信息
function setUserInfo(userInfo){
    sessionStorage.setItem(USER_KEY,userInfo)
}

// 获取信息
function getUserInfo(){
    sessionStorage.getItem(USER_KEY)
}
// 设置删除
function removeAll(){
    sessionStorage.removeItem(TOKEN_KEY)
    sessionStorage.removeItem(USER_KEY)
}

export default{
    setToken,
    getToken,
    setUserInfo,
    getUserInfo,
    removeAll
}