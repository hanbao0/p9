import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import userApi from '../api/user'
import storAuto from '../utils/auth'

export default new Vuex.Store({
  state: {
    token: storAuto.setToken() ? storAuto.setToken() : '',
    userInfo: storAuto.setUserInfo() ? storAuto.setUserInfo() : ''
  },
  mutations: {
    saveToken(state, token) {
      state.token = token
      storAuto.setToken(token)
    },
    saveUsetInfo(state, userInfo) {
      state.userInfo = userInfo
      storAuto.setUserInfo(userInfo)
    }
  },
  actions: {
    // 登录的方法
    logins({ commit }, ruleForm) {
      // 返回一个promise对象
      return new Promise((resolve, reject) => {
        // 使用userapi调用接口
        userApi.doLogin(ruleForm.username, ruleForm.password).then((response) => {
          // 判断是否成功
          if (response.data.flag) {
            // 获取登录接口里面的token
            let token = response.data.data.token
            commit('saveToken', token)
          }
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    getUserInfo({ commit }) {
      return new Promise((resolve, reject) => {
        userApi.doUserInfo().then((response) => {
          console.log(response);
          if (response.data.flag) {
            let userInfo = response.data.data
            commit("saveUsetInfo", userInfo)
          }
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  },
  modules: {
  },
});
