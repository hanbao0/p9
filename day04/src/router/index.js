import Vue from 'vue';
import VueRouter from 'vue-router';


Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'layout',
    redirect:"/home",
    component: () => import( '../components/Layout'),
    children:[
      {
        path: '/home',
        name: 'home',
        component: () => import( '../views/home/'),
      },
      {
        path: '/hygl',
        name: 'hygl',
        meta:{ title:'会员管理'},
        component: () => import( '../views/hygl/'),
      },
      {
        path: '/gysgl',
        name: 'gysgl',
        meta:{ title:'供应商管理'},
        component: () => import( '../views/gysgl/'),
      },
      {
        path: '/spgl',
        name: 'spgl',
        meta:{ title:'商品管理'},
        component: () => import( '../views/spgl/'),
      },
      {
        path: '/yggl',
        name: 'yggl',
        meta:{ title:'员工管理'},
        component: () => import( '../views/yggl/'),
      },
    ]

  },
  {
    path: '/login',
    name: 'login',
    component: () => import( '../views/login/'),
  },


];

const router = new VueRouter({
  routes,
});

export default router;
