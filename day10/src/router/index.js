import Vue from 'vue';
import VueRouter from 'vue-router';
import login from "../views/login.vue"


Vue.use(VueRouter);

const routes = [{
        path: '/login',
        name: 'login',
        component: login,
    },
    {
        path: '/',
        name: '',
        component: () =>
            import ('../views/home.vue'),
    },
];

const router = new VueRouter({
    routes,
});

export default router;