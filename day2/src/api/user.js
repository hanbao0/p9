/**
 * 
 * 用户相关的api接口: 用户登录 获取用户信息 修改密码 退出登录
 * 
 */
//1. 引入request文件
import request from "../utils/request";

//2. 登录接口
let doLogin = (username, password) =>{ 

   return request({
        url: "/user/login",
        method : "POST",
        data : {
            username,
            password
        }
    })
}

//3. 用户信息接口
let doUserInfo = () => {
    return request({
        url: "/user/info",
        method : "GET"
    })
}

//导出封装的接口
export default { 
    doLogin,
    doUserInfo
}
