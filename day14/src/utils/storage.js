//导入项目的全局配置,主要是用到命名空间: namespace 
import config from "../config/index"

export default {
    //获取本地存储的数据
    getItem(key){
        return this.getStorage()[key]
    },
    //设置本地存储的数据
    setItem(key,value){
        let storage = this.getStorage()
        storage[key] = value
        window.localStorage.setItem(config.namespace,JSON.stringify(storage))
    },
    getStorage(){
        return JSON.parse(window.localStorage.getItem(config.namespace)) || {};
    },
    //删除本地存储中的某一项数据
    clearItem(key){
        let storage = this.getStorage();
        delete storage[key];
        window.localStorage.setItem(config.namespace, JSON.stringify(storage));
    },
    //清空本地所有的数据
    clearAll(){
        window.localStorage.clear()
    }
}


