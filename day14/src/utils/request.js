//引入axios
import axios from "axios";

//引入环境变量文件 config/index.js
import config from "../config/index";

//自定义token过期的描述信息
const TOKEN_INVALID = "TOKEN认证失败,请重新登录"

//自定义网络错误的描述信息
const NETWORK_ERROR = "网络请求异常,请稍后重试"

//引入element-ui的Message组件
import {Message} from "element-ui"

//引入router对象
import router from "../router/index"

//创建axios实例对象,并添加公共请求配置
const service = axios.create({
    //请求的公共地址
    baseURL : config.baseApi,
    //请求的超时时间
    timeout : 8000
})

//创建请求拦截器
service.interceptors.request.use((req)=>{
    //通用业务 : token发送 
    let headers = req.headers;
    if(!headers.Authoraztion) headers.Authoraztion = "token"
    return req;
},(err)=>{
    return Promise.reject(err)
})
//创建响应拦截器
service.interceptors.response.use((res)=>{
    //从响应回来的数据获取到请求的状态码 、 数据、 以及提示信息
    let {code, data, msg} = res.data;
    if(code == 200){    //请求数据成功
        return data;
    }else if(code === 40001){   //token过期
        //通过提示框显示token过期的提示信息
        Message.error(TOKEN_INVALID);
        //跳转到登录页
        setTimeout(()=>{
            router.push("/login")
        },1000)
        return Promise.reject(TOKEN_INVALID)
    }else{
        Message.error(NETWORK_ERROR);
        return Promise.reject(NETWORK_ERROR)
    }
},(err)=>{
    return Promise.reject(err)
})
//封装请求核心方法
function request(options){
    //获取传递进来的请求方法,如果没有设置请求的方法,则设置默认的请求方式为get
    options.method = options.method || 'get';
    //解决的问题是: 在通过get请求的数据也可以在发送的时候使用data发送
    if(options.method.toLowerCase() === 'get'){
        //将get方式以及data发送过来的数据,交给params,然后在传递给后台
        options.params = options.data;
    }
    //如果当前的环境是生产环境,则让请求的公共地址是baseApi
    if(config.env == "production"){
        service.defaults.baseURL = config.baseApi;
    }else{
        //如果不是生产环境下,判断有没有开启可以请求数据mock,如果mock为true,则切换mockApi,否则的话则请求真实的接口api
        service.defaults.baseURL = config.mock ? config.mockApi : config.baseApi
    }
    //将promise返回出去
    return service(options)
}
//处理使用this.request.get/post/put/delete/patch这几种请求方法
["get","post","put","delete","patch"].forEach((item)=>{
    request["get"] = (url,data,options)=>{
        return request({
            url,
            data,
            method : item,
            ...options
        })
    }
})

//导出核心请求方法
export default request;

