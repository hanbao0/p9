//获取当前运行的环境  开发环境/测试环境/生产环境 development
const env = process.env.NODE_ENV || "production";

//定义不同环境下请求的代理名称和接口地址
const EnvConfig = {
    //开发环境
    development : {
        //真实的开发阶段的后台api
        baseApi : "/dev-api",
        //模拟数据的接口地址
        mockApi : "https://www.fastmock.site/mock/c1c302e8baed9894c48c17e4738c092e/api"
    },
    //测试环境
    test : {
        baseApi : "/test-api",
        mockApi : "https://test.fastmock.site/mock/76ef040b1066810a6a9e8b7cf636e63d/oa"
    },
    //生产环境
    production : {
        baseApi : "pro-api",
        mockApi : "https://oa.9yuecloud.com"
    } 
}

export default {
    env,
    mock : true,
    namespace : "manager",
    ...EnvConfig["development"]
}

