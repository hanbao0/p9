//引入request
import request from "../utils/request"

//封装部门接口并导出
export default {
    //获取部门列表
    getDeptList(data){
        return request({
            url : "/dept/list",
            method : "GET",
            data
        })
    },
    //创建/编辑/删除 部门接口
    deptOperate(data){
        return request({
            url : "/dept/operate",
            method : "POST",
            data
        })
    },
    //获取所有用户列表
    getUsersAllList(){
        return request({
            url : "/users/all/list",
            method : "GET",
        })
    }
}