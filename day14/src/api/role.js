//引入request
import request from "../utils/request"

//封装角色接口并导出
export default {
    //角色列表接口
    getRoleList(data){
        return request({
            url : "/roles/list",
            method : "GET",
            data 
        })
    },
    //角色创建/编辑/删除
    rolesOperate(data){
        return request({
            url : "/roles/operate",
            method : "POST",
            data
        })
    }
}