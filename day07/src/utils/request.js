//1. 引入axios
import axios from "axios";

//引入store
import store from "../store"

// 引入Emelent-ui的loading
import { Loading } from 'element-ui';

// 创建loading

let loading = {
    // 保存的loading.service的实例化对象
    loadingInstance: null,
    // 创建打开loading的方法
    open: function() {
        if (this.loadingInstance == null) {
            this.loadingInstance = Loading.service({
                target: ".main",
                lock: true,
                text: 'Loading',
                spinner: 'el-icon-loading',
                background: 'rgba(0, 0, 0, 0.7)'
            });
        }
    },
    close: function() {
        if (this.loadingInstance !== null) {
            this.loadingInstance.close()
        }
        this.loadingInstance = null
    }
}

//2. 创建axios实例对象
const request = axios.create({
    //配置请求的公共接口地址
    baseURL: process.env.VUE_APP_BASE_API,
    //配置请求的超时时间
    timeout: 5000
})

//3. 创建请求拦截器
request.interceptors.request.use(function(config) {
    //获取vuex里面保存的token
    let token = store.state.token;
    //通过请求头将token发送给后台
    config.headers.token = token;
    loading.open()
    return config;
}, function(error) {

    return Promise.reject(error);
});

//4. 创建响应拦截器
request.interceptors.response.use(function(response) {
    loading.close()
    return response;
}, function(error) {

    return Promise.reject(error);
});


//5. 导出axios实例对象
export default request