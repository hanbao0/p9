import request from '../utils/request'
// 登录接口
let doLogin = (username,password) => {
    return request({
        url: '/user/login',
        method: 'POST',
        data: {
            username,
            password
        }
    })
}

let doUserInfo = () => {
    return request({
        url: '/user/info',
        method: "GET"
    })
}

export default {
    doLogin,
    doUserInfo
}

