import Vue from 'vue';
import VueRouter from 'vue-router';
// import Home from '../views/Home.vue';

Vue.use(VueRouter);

const routes = [
  // {
  //   path: '/',
  //   name: 'Home',
  //   component: Home,
  // },
  {
    path: '/login',
    name: 'Login',
    component: () => import( '../views/login'),
  },

  {
    path: '/',
    name: 'Layout',
    redirect:'/home',
    component: () => import( '../components/Layout.vue'),
    children:[
          {
            path: '/home',
            name: 'home',
            component: () => import( '../views/home/'),
          },
          {
            path: '/member',
            name: 'member',
            meta :{title:'会员管理'},
            component: () => import( '../views/member/'),
          },
          {
            path: '/supplier',
            name: 'supplier',
            meta :{title:'供应商管理'},
            component: () => import( '../views/supplier/'),
          },
          {
            path: '/goods',
            name: 'goods',
            meta :{title:'商品管理'},
            component: () => import( '../views/goods/'),
          },
          {
            path: '/staff',
            name: 'staff',
            meta :{title:'员工管理'},
            component: () => import( '../views/staff/'),
          },
          
        ],
  },
  // {
  //   path: '/member',
  //   component: () => import( '../components/Layout.vue'),
  //   children:[
  //     {
  //       path: '/',
  //       meta :{title:'会员管理'},
  //       component: () => import( '../views/member'),
  //     }
  //   ]
  // },
  // {
  //   path: '/supplier',
  //   component: () => import( '../components/Layout.vue'),
  //   children:[
  //     {
  //       path: '/',
  //       meta :{title:'供应商管理'},
  //       component: () => import( '../views/supplier'),
  //     }
  //   ]
  // },
  // {
  //   path: '/goods',
  //   component: () => import( '../components/Layout.vue'),
  //   children:[
  //     {
  //       path: '/',
  //       meta :{title:'商品管理'},
  //       component: () => import( '../views/goods'),
  //     }
  //   ]
  // },
  // {
  //   path: '/staff',
  //   component: () => import( '../components/Layout.vue'),
  //   children:[
  //     {
  //       path: '/',
  //       meta :{title:'员工管理'},
  //       component: () => import( '../views/staff'),
  //     }
  //   ]
  // },
  
 
];

const router = new VueRouter({
  routes,
});

export default router;
