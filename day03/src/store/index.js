
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import userApi from '../api/user'
import storage from '../utils/auth'

export default new Vuex.Store({
  state: {
    //定义token存储本地内容
    token: storage.getToken() ? storage.getToken() : "",
    userInfo: storage.getuserInfo() ? storage.getuserInfo() : ""
  },
  mutations: {
    saveToken(state,token){
      state.token=token
      storage.setToken(token)
    },
    saveUserInfo(state,userInfo){
      state.userInfo=userInfo
      storage.setUsetInfo(userInfo)
    }
  },
  actions: {
    // 登录
    login( {commit} ,loginList){
      return new Promise( ( resolve,reject)=>{
        userApi.doLogin( loginList.username,loginList.password).then(response=>{
          if(response.data.flag){
            let token=response.data.data.token
            commit("saveToken",token)
          }
          resolve(response)
        }).catch(error=>{
          reject(error)
        })
      })
    },

    getuserInfo({commit}){
      return new Promise( ( resolve,reject)=>{
        userApi.doUserInfo().then(response=>{
          if(response.data.flag){
            let userInfo=response.data.data
            commit("saveUserInfo",userInfo)
          }
          resolve(response)
        }).catch(error=>{
          reject(error)
        })
      })
    },


  },
 
  
  modules: {
  },
});
