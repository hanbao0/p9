// 1.引入axios、
import axios from 'axios'
import store from '../store/index'
// 2创建axiso实例对象
const request = axios.create({
  baseURL: process.env.VUE_APP_BASE_API,
  timeout: 5000
})
// 3创建请求拦截器
request.interceptors.request.use(function (config) {
  // 获取vuex里面的token值
  let token = store.state.token
  // 通过请求头将tiken的值提交到后台
  config.headers.token = token

  return config;
}, function (error) {
  return Promise.reject(error);
});
   
// 4创建响应拦截器
request.interceptors.response.use(function (response) {
  return response;
}, function (error) {
  return Promise.reject(error);
});


// 5导出
export default request