// 封装存储  设置token和用户信息

//定义常量 （好维护）
const TOKEN_KEY="token"
const USER_KEY="user"

// 存储token
function setToken(token){
    sessionStorage.setItem(TOKEN_KEY,token)
}
// 获取token
function getToken(){
   return sessionStorage.getItem(TOKEN_KEY)
}

// 存储用户信息
function setUsetInfo(usetInfo){
    sessionStorage.setItem(USER_KEY,JSON.stringify(usetInfo))
}
// 获取用户信息
function getuserInfo(){
   return  JSON.parse(sessionStorage.getItem(USER_KEY))
}

//删除用户信息
function removeTokenAndUse(){
    sessionStorage.removeItem(TOKEN_KEY)
    sessionStorage.removeItem(USER_KEY)
}

export default{
    setToken,
    getToken,
    setUsetInfo,
    getuserInfo,
    removeTokenAndUse
}