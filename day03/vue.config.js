module.exports = {
    //解决vue项目打包白屏问题
    publicPath: './',
    // 对服务器进行配置
    devServer: {
        // 不开启HTTPS协议
        https: false,
        // 配置跨域
        proxy: {
            // 代理名称
            [process.env.VUE_APP_BASE_API]: {
                // 要跨域的地址
                target: process.env.VUE_APP_SERVIC_URL,
                // 开启跨域
                changeOrigin: true,
                // 路径重写
                pathRewrite: {
                    ['^' + process.env.VUE_APP_BASE_API]: ''
                }
            }
        }
    },

    lintOnSave: false,
    productionSourceMap: false
}