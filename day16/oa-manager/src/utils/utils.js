
export default {
    formateDate(date, rule) {
        let fmt = rule || 'yyyy-MM-dd hh:mm:ss'
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, date.getFullYear())
        }
        const o = {
            // 'y+': date.getFullYear(),
            'M+': date.getMonth() + 1,
            'd+': date.getDate(),
            'h+': date.getHours(),
            'm+': date.getMinutes(),
            's+': date.getSeconds()
        }
        for (let k in o) {
            if (new RegExp(`(${k})`).test(fmt)) {
                const val = o[k] + '';
                fmt = fmt.replace(RegExp.$1, RegExp.$1.length == 1 ? val : ('00' + val).substr(val.length));
            }
        }
        return fmt;
    },
    generatorRoutes(menu){
        let routes = [];
        function deepList(menu){
      
          while(menu.length){// 父   2子
            let item = menu.pop()
            
            if(item.action){
              routes.push({
                path : item.path,
                component : item.component,
                name : item.component,
                meta : {title : item.menuName}
              })
            }
      
            if(item.children && item.children.length > 0){
              deepList(item.children)
            }
          }
      
        }
      
        deepList(menu)  
      
        return routes;
      } 
}