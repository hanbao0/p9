/**
 * 封装菜单相关接口
 * @author : yangmr
 * @date : 2021-11-10
*/

//1. 引入request
import request from "../utils/request"

//2. 封装菜单接口并导出
export default {
    //菜单列表接口
    getMenuList(data){
        return request({
            url : "/menu/list",
            method : "GET",
            data
        })
    },

    //菜单 新增/编辑/删除 接口
    createMenu(data){
        return request({
            url : "/menu/operate",
            method : "POST",
            data
        })
    }
}